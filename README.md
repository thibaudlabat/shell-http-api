# Shell HTTP API

Transform any Linux command into an HTTP API

# TO-DO
- Support bash raw socket
- Detect/Support different netcat binaries
- Support ash/busybox
- Support openwrt
- real HTTP server
- api keys
- api paths and multiple commands
- proper logs